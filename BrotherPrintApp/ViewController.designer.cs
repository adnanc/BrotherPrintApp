// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace BrotherPrintApp
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PrintButton { get; set; }

        [Action ("PrintButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void PrintButton_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (PrintButton != null) {
                PrintButton.Dispose ();
                PrintButton = null;
            }
        }
    }
}