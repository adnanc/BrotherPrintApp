﻿using System;

using UIKit;

namespace BrotherPrintApp
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            
            BRPtouchPrintInfo printInfo = new BRPtouchPrintInfo();

            printInfo.StrPaperName = "A4_CutSheet";
            printInfo.NPrintMode = (int)1;// PRINT_FIT;
            printInfo.NDensity = 5;
            printInfo.NOrientation = (int)1; //Orientation.ORI_PORTRATE;
            printInfo.NHalftone = (int)1;//Halftone.HALFTONE_ERRDIF;
            printInfo.NHorizontalAlign = (int)1;//HorizontalAlign.ALIGN_CENTER;
            printInfo.NVerticalAlign = (int)1;//VerticalAlign.ALIGN_MIDDLE;
            printInfo.NPaperAlign = (int)1;//PaperAlign.PAPERALIGN_LEFT;
            printInfo.NExtFlag |= 0;

            // Initialize PJ-673 printer
            BRPtouchPrinter printer = new BRPtouchPrinter();
            printer.SetPrinterName("QL-820NWB");
            printer.SetPrintInfo(printInfo);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
